global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global print_error
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy

section .text

%define WRITE 1
%define STDOUT 1
%define STDERR 2
%define SYM_LENGTH 1

%define SPACE 0x20
%define LINE_FEED 0xA
%define TAB 0x9

; принимает указатель на нуль-терминированную строку, выводит ее в stderr с переводом строки
print_error:
    push rdi
    call string_length
    mov  rdx, rax
    pop  rsi
    mov  rax, WRITE
    mov  rdi, STDERR
    syscall
    call print_newline
    ret

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov  rdx, rax
    pop rsi
    mov  rax, WRITE
    mov  rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
     mov  rax, WRITE
     mov  rdi, STDOUT
     mov  rsi, rsp
     mov  rdx, SYM_LENGTH
     syscall
     pop rax
     ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    mov r9, rsp
    push 0
   .div_loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        jne .div_loop
        mov rdi, rsp
        push r9 ; save rsp
        call print_string
        pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    print_int:
    cmp rdi, 0
    jge .print
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .print:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
    xor rax, rax
    .loop:
        mov r8b, byte[rsi+rax] ; 2nd str symb
        cmp byte[rdi+rax], r8b ; cmp with 1st str symb
        jne .fail
        cmp byte[rdi+rax], 0
        je .ok
        inc rax
        jmp .loop
    .ok:
        mov rax, 1
        ret
    .fail:
        mov rax, 0
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
     mov  rax, 0
     mov  rdi, 0
     mov  rsi, rsp
     mov  rdx, 1
     syscall
     pop rax
     ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_line:
    mov r9, rsi
    xor r8, r8
    .loop:
        push rdi
        call read_char
        pop rdi

        cmp rax, SPACE
        je .check
        cmp rax, TAB
        je .check
        cmp rax, LINE_FEED
        je .check

    .continue:
        cmp rax, 0
        je .ok

        cmp r8, r9
        je .fail

        mov byte[rdi + r8], al
        inc r8
        jmp .loop

    .check:
        test r8, r8
        je .loop
        cmp rax, 0x20 ;проверка на пробел в середине слова
        je .continue
    .ok:
        mov rax, rdi
        mov rdx, r8
        mov byte[rdi + r8], 0
        inc r8
        ret
    .fail:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10

    .loop:
        xor r9, r9
        mov r9b, byte[rdi]

        cmp r9b, '0'
        jl .end
        cmp r9b, '9'
        jg .end

        inc rcx
        mul r8
        sub r9b, '0'
        add rax, r9
        inc rdi
        jmp .loop
    .end:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    je .sign
    jmp parse_uint
    .sign:
        inc rdi
        call parse_uint
        test rdx, rdx
        je .end
        inc rdx
        neg rax
     .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r8, r8
    .loop:
        mov r8b,  byte[rdi + rax] ; 1st string symb
        mov byte[rsi + rax], r8b  ;copy it
        inc rax
        cmp rax, rdx
        jge .fail
        cmp r8b, 0
        je .ok
        jmp .loop
    .ok:
        ret
    .fail:
        xor rax, rax
        ret
