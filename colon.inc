%define NEXT_ADDRESS 0

%macro colon 2
    %%current_address: dq NEXT_ADDRESS
    %define NEXT_ADDRESS %%current_address
    db %1, 0
    %2:
%endmacro