ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm dict.o lib.o colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

program: main.o lib.o dict.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm *.o temp

