%include "lib.inc"

section .text

global find_word

%define SHIFT 8

find_word:
    .loop:
        push rdi
        push rsi

        add rsi, SHIFT
        call string_equals

        test rax, rax
        pop rsi
        pop rdi
        jne .ok
        mov rsi, qword[rsi]
        test rsi, rsi
        je .fail
        jmp .loop
    .ok:
        mov rax, rsi
        ret
    .fail:
        xor rax, rax
        ret