%include "lib.inc"
%define SHIFT 8
%define BUFFER_SIZE 255

section .data
%include "words.inc"

error_message: db 'Error: your word is too long', 0
word_not_found: db 'Error: such key is not found', 0
new_word: db 0

section .text

extern find_word

global _start
    _start:
        mov rdi, new_word
        mov rsi, BUFFER_SIZE
        call read_line
        test rax, rax
        jne .find
    .error:
        mov rdi, error_message
        call print_error
        jmp .exit
    .find:
        mov rdi, rax
        mov rsi, NEXT_ADDRESS
        call find_word
        test rax, rax
        je .not_found
    .found:
        mov rdi, rax
        add rdi, SHIFT
        push rdi
        call string_length
        pop rdi
        inc rax
        add rdi, rax
        call print_string
        call print_newline
        jmp .exit
    .not_found:
        mov rdi, word_not_found
        call print_error
    .exit:
        mov rdi, 60
        jmp exit



